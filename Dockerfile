FROM haugene/transmission-openvpn:latest AS base-amd64
FROM haugene/transmission-openvpn:latest-arm64 AS base-arm64

FROM base-$TARGETARCH
