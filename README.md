# docker-transmission-openvpn

This project builds a multi-arch (amd64/arm64) docker image from https://github.com/haugene/docker-transmission-openvpn

The image is built every day from latest tags base image.

## Usage
`docker run neonox31/transmission-openvpn`
